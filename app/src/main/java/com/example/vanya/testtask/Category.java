package com.example.vanya.testtask;

/**
 * Created by vanya on 09.01.15.
 */
public class Category {
    private String id, name;

    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.example.vanya.testtask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanya on 09.01.15.
 */
public class ArticleListFragment extends Fragment{
    private Category category;
    private List<Article> articles = new ArrayList<>();
    private ArticleAdapter articleAdapter;
    private Activity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_list, null);

        TextView tvCategoryName = (TextView) view.findViewById(R.id.tvCategoryTitle);
        tvCategoryName.setText(category.getName().toUpperCase());

        ListView lvArticles = (ListView) view.findViewById(R.id.lvArticles);

        articleAdapter = new ArticleAdapter(activity, 0);
        lvArticles.setAdapter(articleAdapter);

        loadArticles();

        return view;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    private void loadArticles() {
        RequestQueue queue = TestApplication.getRequestQueue();

        final String articlesUrl = String.format("http://figaro.service.yagasp.com/article/header/%s", category.getId());

        JsonArrayRequest objectRequest = new JsonArrayRequest(articlesUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    JSONArray articlesArray = (JSONArray) response.get(1); //because of json
                    for (int i = 0; i < articlesArray.length(); i++) {
                        JSONObject articleObject = (JSONObject) articlesArray.get(i);

                        String articleId = articleObject.getString("id");
                        String articleTitle = articleObject.getString("title");
                        String articleSubTitle = articleObject.getString("subtitle");

                        JSONObject thumbObject = articleObject.getJSONObject("thumb");
                        String imageUrl = thumbObject.getString("link");

                        Article article = new Article(articleId, articleTitle, articleSubTitle, imageUrl);

                        articles.add(article);
                    }

                    articleAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    //some error message
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //error handling
                    }
                });

        queue.add(objectRequest);
    }

    private class ArticleAdapter extends ArrayAdapter<Article> {
        private LayoutInflater inflater;

        public ArticleAdapter(Context context, int resource) {
            super(context, resource);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_article, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final Article article = getItem(position);
            holder.articleTitle.setText(article.getTitle());
            holder.articleSubTitle.setText(article.getSubTitle());

            String thumbUrl = String.format(article.getThumbUrl(), 300, 300);
            ImageLoader.getInstance().displayImage(thumbUrl, holder.articleThumb, new DisplayImageOptions.Builder().resetViewBeforeLoading(true)
                    .cacheOnDisc(true)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ArticleActivity.class);
                    intent.putExtra(TestApplication.ARTICLE_EXTRA, article);
                    startActivity(intent);
                }
            });

            return convertView;
        }

        @Override
        public Article getItem(int position) {
            return articles.get(position);
        }

        @Override
        public int getCount() {
            return articles.size();
        }

        private class ViewHolder {
            TextView articleTitle, articleSubTitle;
            ImageView articleThumb;

            ViewHolder(View view) {
                articleTitle = (TextView) view.findViewById(R.id.tvArticleTitle);
                articleSubTitle = (TextView) view.findViewById(R.id.tvArticleSubtitle);
                articleThumb = (ImageView) view.findViewById(R.id.ivThumb);
            }
        }
    }
}

package com.example.vanya.testtask;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String CATEGORIES_URL = "http://figaro.service.yagasp.com/article/categories";

    private ViewPager vpArticles;
    private List<ArticleListFragment> fragments = new ArrayList<>();
    private FragmentStatePagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vpArticles = (ViewPager) findViewById(R.id.vpArticles);
        adapter = new CategoryAdapter(getSupportFragmentManager());
        vpArticles.setAdapter(adapter);

        loadCategories();
    }

    private void loadCategories() {
        RequestQueue queue = TestApplication.getRequestQueue();
        JsonArrayRequest objectRequest = new JsonArrayRequest(CATEGORIES_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject categoryObject = (JSONObject) response.get(i);
                        JSONObject subcategory = (JSONObject) categoryObject.getJSONArray("subcategories").get(0);

                        String subcategoryId = subcategory.getString("id");
                        String subcategoryName = subcategory.getString("name");
                        Category category = new Category(subcategoryId, subcategoryName);
                        TestApplication.getCategories().add(category);

                        ArticleListFragment fragment = new ArticleListFragment();
                        fragment.setCategory(category);
                        fragments.add(fragment);
                    }

                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    //some error message
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    //error handling
                    }
                });

        queue.add(objectRequest);
    }


    private class CategoryAdapter extends FragmentStatePagerAdapter {

        public CategoryAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}

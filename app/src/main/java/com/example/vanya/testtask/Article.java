package com.example.vanya.testtask;

import java.io.Serializable;

/**
 * Created by vanya on 09.01.15.
 */
public class Article implements Serializable{
    private String id;
    private String internalId;

    public Article(String id, String title, String subTitle, String thumbUrl) {
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
        this.thumbUrl = thumbUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    private String title;
    private String subTitle;
    private String thumbUrl;

}

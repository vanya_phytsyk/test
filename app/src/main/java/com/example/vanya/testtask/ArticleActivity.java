package com.example.vanya.testtask;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * Created by vanya on 09.01.15.
 */
public class ArticleActivity extends ActionBarActivity {

    private Article article;
    private TextView tvArticleTitle;
    private TextView tvArticleSubtitle;
    private TextView tvAuthorNameDate;
    private TextView tvArticleText;
    private ImageView ivPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_article);

        Intent intent = getIntent();
        article = (Article) intent.getSerializableExtra(TestApplication.ARTICLE_EXTRA);

        tvArticleTitle = (TextView) findViewById(R.id.tvArticleTitle);
        tvArticleSubtitle = (TextView) findViewById(R.id.tvArticleSubtitle);
        tvAuthorNameDate = (TextView) findViewById(R.id.tvAuthor);
        tvArticleText = (TextView) findViewById(R.id.tvArticleText);
        ivPhoto = (ImageView) findViewById(R.id.ivThumb);

        loadArticle();
    }

    private void loadArticle() {
        RequestQueue queue = TestApplication.getRequestQueue();

        final String articlesUrl = String.format("http://figaro.service.yagasp.com/article/%s", article.getId());

        JsonObjectRequest objectRequest = new JsonObjectRequest(articlesUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String authorName = response.getString("author");
                    String articleText = response.getString("content");
                    String articleSubtitle = response.getString("subtitle");
                    String articleTitle = response.getString("title");

                    String date = new SimpleDateFormat("DD/MM/yyyy").format(response.getLong("date"));

                    JSONArray photosArray = response.getJSONArray("photos");
                    String imageUrl = ((JSONObject) photosArray.get(0)).getString("url");

                    tvArticleTitle.setText(articleTitle);
                    tvArticleSubtitle.setText(articleSubtitle);
                    tvArticleText.setText(Html.fromHtml(articleText));
                    tvAuthorNameDate.setText(authorName + ", " + date);

                    ImageLoader.getInstance().displayImage(String.format(imageUrl, 640, 480), ivPhoto, new DisplayImageOptions.Builder().resetViewBeforeLoading(true)
                            .cacheOnDisc(true)
                            .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                            .bitmapConfig(Bitmap.Config.RGB_565)
                            .build());

                } catch (JSONException e) {
                    //some error message
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //error handling
                    }
                });

        queue.add(objectRequest);
    }
}

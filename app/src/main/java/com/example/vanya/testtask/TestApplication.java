package com.example.vanya.testtask;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanya on 09.01.15.
 */
public class TestApplication extends Application {
    private static RequestQueue requestQueue;
    private static List<Category> categories = new ArrayList<>();
    public static final String ARTICLE_EXTRA = "article_extra";

    @Override
    public void onCreate() {
        super.onCreate();
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
    }

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public static List<Category> getCategories() {
        return categories;
    }
}
